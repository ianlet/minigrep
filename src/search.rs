pub fn search_sensitive<'a>(query: &str, content: &'a str) -> Vec<&'a str> {
    content
        .lines()
        .filter(|line| line.contains(query))
        .collect()
}

pub fn search_insensitive<'a>(query: &str, content: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();
    content
        .lines()
        .filter(|line| line.to_lowercase().contains(&query))
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    const QUERY: &'static str = "duct";

    const CONTENT_WITHOUT_QUERY: &'static str = "potato";
    const CONTENT_WITH_QUERY_SAME_CASE: &'static str = "\
Rust:
safe, fast, productive.
Pick three.";
    const CONTENT_WITH_QUERY_DIFFERENT_CASE: &'static str = "\
Duct tape is great
And you can fix anything with";

    #[test]
    fn content_without_query_search_sensitive_does_not_find_query() {
        let result = search_sensitive(QUERY, CONTENT_WITHOUT_QUERY);

        let no_result: Vec<&str> = vec![];
        assert_eq!(no_result, result);
    }

    #[test]
    fn content_with_query_different_case_search_sensitive_does_not_find_query() {
        let result = search_sensitive(QUERY, CONTENT_WITH_QUERY_DIFFERENT_CASE);

        let no_result: Vec<&str> = vec![];
        assert_eq!(no_result, result);
    }

    #[test]
    fn content_with_query_same_case_search_sensitive_finds_query() {
        let result = search_sensitive(QUERY, CONTENT_WITH_QUERY_SAME_CASE);

        let expected_result = vec!["safe, fast, productive."];
        assert_eq!(expected_result, result);
    }

    #[test]
    fn content_without_query_search_insensitive_does_not_find_query() {
        let result = search_insensitive(QUERY, CONTENT_WITHOUT_QUERY);

        let no_result: Vec<&str> = vec![];
        assert_eq!(no_result, result);
    }

    #[test]
    fn content_with_query_same_case_search_insensitive_finds_query() {
        let result = search_insensitive(QUERY, CONTENT_WITH_QUERY_SAME_CASE);

        let expected_result = vec!["safe, fast, productive."];
        assert_eq!(expected_result, result);
    }

    #[test]
    fn content_with_query_different_case_search_insensitive_finds_query() {
        let result = search_insensitive(QUERY, CONTENT_WITH_QUERY_DIFFERENT_CASE);

        let expected_result = vec!["Duct tape is great"];
        assert_eq!(expected_result, result);
    }
}

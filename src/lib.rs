pub mod config;
mod search;

use std::error::Error;
use std::fs::File;
use std::io::prelude::*;

use config::Config;
use search::search_sensitive;

pub fn run(config: Config) -> Result<(), Box<Error>> {
    let content = read_file(&config.filename)?;
    for line in search_sensitive(&config.query, &content) {
        println!("{}", line);
    }
    Ok(())
}

fn read_file(filename: &String) -> Result<String, Box<Error>> {
    let mut file = File::open(filename).expect("file not found");
    let mut content = String::new();
    file.read_to_string(&mut content)?;
    Ok(content)
}
